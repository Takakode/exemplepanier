# Exemple de gestion d'un panier / Sample of a basshopping list

Ce petit projet et un exemple qu'on m'a demandé de faire.
Je vais essayer de faire quelque chose de bien en peu de temps.

This little project is an sample that we asked me to do.
I will try to do something good with a minimum of time.

## Technos
- Php
- Js
- Mysql

## Test the project

Vous pouvez tester le projet sur votre machine si vous utilisez un serveur Apache, ou avec Wamp/Mamp.
N'oubliez pas de mettre à jour la base de données avec le fichier : setDatabase.

You can use the project with Mamp/Wamp (with an Apache server). Don't forget to update the mysql database with the setDatabase.sql file.