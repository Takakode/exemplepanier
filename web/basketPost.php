<?php
if($_SERVER["CONTENT_TYPE"] == "application/json;charset=UTF-8") {
    $json = file_get_contents('php://input');
    echo $json;
    if( empty($json) && $json != Null) {
        //echo $_POST["x"];
        echo "Server : Error missing json data";
    } else {
        $objJson = json_decode($json);
        print_r($objJson);
        if (empty($objJson)) {
            header("HTTP/1.0 442 Sent wrong data");
            echo "Server : Error json data empy";
        }
    }
} else {
    header("HTTP/1.0 400 Bad Request");
    echo "Server : Error wrong request";
}
