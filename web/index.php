<?php include("parts/head.php");?>
<div class="container">
    <div class="col">
        <h1>Index page</h1>
        <p>Welcome to the basket sample</p>
        <ul>
            <li>
                <a href="products.php">products list</a>
            </li>
            <li>
                <a href="basket.php">basket</a>
            </li>
        </ul>
    </div>
</div>
<?php include("parts/footer.php");?>