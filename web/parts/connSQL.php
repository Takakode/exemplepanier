<?php
//

function connectToDTB() {
    $user = 'root';
    $password = 'root';
    $db = 'sample_basket';
    $host = 'localhost';
    $port = 8889;
    try {
        $conn = new PDO("mysql:host=$host:$port;dbname=$db", $user, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Database : Connected successfully";
        return $conn;
        }
    catch(PDOException $e)
        {
        echo error_log("Database : Connection failed: " . $e->getMessage());
        return FALSE;
        }
}
    // @NOTE don't forget to close the connection
?>