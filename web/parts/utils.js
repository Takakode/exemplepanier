function getShoppingList(){
    var allCookies = document.cookie;
    allCookies = allCookies.split(";");
    var cookie;
    for (cookie of allCookies) {
        if( cookie.indexOf(" shoppingList") != -1){
            var cookieRes = cookie.split("=")[1];
            if(cookieRes.length <=2) {
                return null
            } else {
                return JSON.parse(cookieRes);
            }
        }
    }
    return null;
}

function addProduct(id_product) {
    list_products =  getShoppingList() == null ? {} : getShoppingList();
    if( typeof(list_products[id_product]) == 'undefined' ) {
        list_products[id_product] = 1; 
    } else {
        list_products[id_product] = list_products[id_product] + 1;
    }
    console.log(list_products);
    changeNumber(id_product);
    saveList();
}

function delProduct(id_product) {
    list_products =  getShoppingList() == null ? {} : getShoppingList();
    console.log(list_products);
    if( typeof(list_products[id_product]) == 'undefined') {
        list_products[id_product] = 0; 
    } else if (list_products[id_product] <= 1) {
        console.log(list_products[id_product]);
        delete list_products[id_product];

    } else {
        console.log(list_products[id_product]);
        list_products[id_product] = list_products[id_product] -1;
    }

    console.log(list_products);
    changeNumber(id_product);
    saveList();
}

function changeNumber(id_product) {
    if( list_products[id_product] > 0){
        document.getElementById("numberProduct_"+id_product).style.visibility= 'visible';
        document.getElementById("numberProduct_"+id_product).innerHTML= ""+list_products[id_product];
    } else {

        document.getElementById("numberProduct_"+id_product).style.visibility= 'hidden';
    }
}

function saveList(){
    document.cookie="shoppingList="+JSON.stringify(list_products);
    console.log("shopping list saved in cookie");
}
