<?php include("parts/head.php"); ?>

<div class="container">
    <div class="row">
        <div class="col">
            <h1>Products List</h1>
            <p>Here is the list of product</p>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <?php 
            // we print the products list with the cookie and database informations
            include_once("parts/connSQL.php");
            $conn = connectToDTB();
            ?>

            <ul class="list-group">
            <?php
                $query = "SELECT * FROM products";
                $res = $conn->prepare($query);
                $res->execute();
                $products = $res->fetchAll();

                if(!empty($_COOKIE["shoppingList"]) and $_COOKIE["shoppingList"] != "" and $_COOKIE["shoppingList"] !="{}" ) {
                    $shoppingListJson = json_decode($_COOKIE["shoppingList"],true);
                }

                foreach($products as $product) {
                    if(isset($shoppingListJson)) {
                        if( isset($shoppingListJson[$product["id_product"]]) and $shoppingListJson[$product["id_product"]] > 0 ){
                            $number = $shoppingListJson[$product["id_product"]];
                            $visibility = "visible";
                        } else {
                            $number ="";
                            $visibility = "hidden";
                        }
                    } else {
                        $number = "";
                        $visibility = "hidden";
                    }
                    // we echo every product information and their eventlistener (del,add) cf utils.js
                    echo "<li class='list-group-item'>"
                    ."<div class='d-flex w-100 justify-content-between'>"
                    ."<h2>"
                    .ucfirst($product["title"])." "
                    ."</h2>"
                    ."<small>"
                    .$product["price"]." "
                    ." <span class='badge badge-primary badge-pill' style='display:inline;visibility:".$visibility."' id='numberProduct_".$product["id_product"]."'>".$number."</span>"
                    ."<button type='button' class='btn btn-success' onclick=\"addProduct('".$product["id_product"]."')\">+</button>"
                    ."<button type='button' class='btn btn-danger' onclick=\"delProduct('".$product["id_product"]."')\">-</button>"
                    ."</small>"
                    ."</div>"
                    .ucfirst($product["description"])." "
                    ."</li>";
                }
            ?>
            </ul>
        </div>
    </div>
    </br>
    <div class="row">
        <div class="col">
            <button type='button' class="btn btn-primary" onclick="goToBuy()">Send</button>
        </div>
    </div>
    <div class="row">
        <div class="col" id="server_answer_list" hidden>
        </div>
    </div>
</div>


<script src="parts/utils.js"></script>
<script>
var list_products={};

// let's go to the next page 
// (the cookie will keep the informations of the shopping list)
function goToBuy() {
    var currentUrl = window.location.href.split("/");
    currentUrl = currentUrl[0];
    window.location.href = currentUrl+"/basket.php";
}
</script>
<?php include("parts/footer.php"); ?>