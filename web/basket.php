<?php include("parts/head.php"); ?>

<div class="container">
    <div class="col">
        <h1>Your shopping list</h1>
        <p>Here you can see a resume of your shopping list</p>
    </div>
</div>

<div class="container">
    <div class="col">
            <ol id="shopping_list" class="list-group">
                <?php
                    // the shopping list is in the cookie, let's print it
                    if( !empty($_COOKIE["shoppingList"]) and $_COOKIE["shoppingList"] != "" and $_COOKIE["shoppingList"] !="{}" ){
                        $shoppingListJson = json_decode($_COOKIE["shoppingList"]);
                        include_once("parts/connSQL.php");
                        $conn = connectToDTB();
                        $totalShoppingList = 0;

                        foreach ($shoppingListJson as $key => $product){
                            if ( $product > 0 ) {
                                $query = $conn->prepare("SELECT * FROM products WHERE id_product=".$key.";");
                                $query->execute();
                                $result =  $query->fetchAll()[0];
                                echo "<li class='list-group-item' id='product_".$key."'>"
                                ."<div class='d-flex w-100 justify-content-between'>"
                                ."<h2>"
                                .ucfirst($result["title"])
                                ."</h2>"
                                ."<small>"
                                ."<p id='productPrice_".$key."' data-priceunit='".$result["price"]."'>"
                                .$result["price"]*$product."€"
                                ."</p>"
                                ." <span class='badge badge-primary badge-pill' id='numberProduct_".$key."'>".$product."</span>"
                                ."</small>"
                                ."</div>"
                                ."<p>"
                                .ucfirst($result["description"])
                                ."</p>"
                                ."<button type='button' class='btn btn-danger' onclick=\"delUpdateListProducts('".$key."')\">-</button>"
                                ."</li>";

                                $totalShoppingList+= $result["price"]*$product;
                            }
                        }
                        echo "<p id='totalPrice' data-totalprice='".$totalShoppingList."' >Total: ".$totalShoppingList."€</p>";

                    } else {
                        // there is nothing damn
                        echo "It's pretty empty here, you don't want to buy some stuff?";
                    }
                ?>
            </ol>
            </br>
        <button id="send_shopping_list" type="button" class="btn btn-primary" >Pay</button>
    </div>
</div>
</br>
<div class="container">
    <div class="col">
        <div id="server_answer_list" style='visibility:hidden'>
        </div>
    </div>
</div>

<script src="parts/utils.js"></script>
<script>
var list_products={};

// I think it's the simpliest way to add an eventlistener with argument on multiple element 
function delUpdateListProducts(idProduct) {
    delProduct(idProduct); // note: it's alway - 1
    console.log("price updating...");
    // there 2 places where to update data : the product number and the total
    var priceBalise = document.getElementById("productPrice_" + idProduct);
    var numberProduct = document.getElementById("numberProduct_" + idProduct);

    if(numberProduct.innerText){
        priceBalise.innerHTML= (parseFloat(priceBalise.dataset.priceunit) * parseFloat(numberProduct.innerText))+"€";
    } else {
        priceBalise.innerHTML=""
        var liProduct = document.getElementById("product_"+idProduct);
        liProduct.parentNode.removeChild(liProduct);
    }

    var totalList =  document.getElementById("totalPrice");
    var newTotal = parseFloat(totalList.dataset.totalprice) -  parseFloat(priceBalise.dataset.priceunit);

    totalList.dataset.totalprice =newTotal;
    totalList.innerText = "Total: " + newTotal +"€";
    console.log("price updated");
}


// send the list to the server to start the buying process
// note: we only send id of product and number not the price
// the server add to calcul the price on its side so the client can't cheat
function sendList(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
            list_products = [];
            document.cookie = "shoppingList=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            var answer = document.getElementById("server_answer_list");
            answer.innerHTML = "Thank you for buying our stuffs.";
            answer.classList.add("alert");
            answer.classList.remove("alert-danger");
            answer.classList.add("alert-success");
            answer.style.visibility = "visible";
            setTimeout(function(){
                answer.style.visibility = "hidden";
                document.getElementById("shopping_list").innerHTML="";
            }, 2000);

        } else if (this.readyState == 4 && this.status == 442){
            var answer = document.getElementById("server_answer_list");
            answer.innerHTML = "Your list is empty.";
            answer.classList.add("alert");
            answer.classList.remove("alert-success");
            answer.classList.add("alert-danger");
            answer.style.visibility = "visible";
            setTimeout(function(){
                answer.style.visibility = "hidden";
            }, 2000);
        } else {
            var answer = document.getElementById("server_answer_list");
            answer.innerHTML = "An error happened, sorry we will fix it.";
            answer.classList.add("alert");
            answer.classList.remove("alert-success");
            answer.classList.add("alert-danger");
            answer.style.visibility = "visible";
            setTimeout(function(){
                answer.style.visibility = "hidden";
            }, 2000);
        }
    };
    xhttp.open("POST", "basketPost.php", true);
    list_products =  getShoppingList();
    var myJSON = JSON.stringify(list_products);
    console.log(myJSON);
    xhttp.setRequestHeader("Content-type", "application/json;charset=UTF-8");
    xhttp.send(myJSON);
    console.log("sent");
}

// most correct way to add an eventListener
var sendButton = document.getElementById("send_shopping_list");
sendButton.addEventListener("click",sendList,false);

</script>

<?php include("parts/footer.php"); ?>